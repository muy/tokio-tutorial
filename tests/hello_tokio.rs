use tokio_tutorial::hello_tokio;

#[tokio::test]
pub async fn test_hello_tokio() {
    let r = hello_tokio::run().await;
    assert!(r.is_ok());
}
