use mini_redis::{client, Result};

pub async fn run() -> Result<()> {
    // 打开连接
    let mut client = client::connect("127.0.0.1:6379").await?;

    // 设置 `hello` 键的值
    client.set("hello", "world".into()).await?;

    // 读取 `hello` 键的值
    let result = client.get("hello").await?;
    println!("从服务器获取到值，结果是：{:?}", result);

    Ok(())
}
