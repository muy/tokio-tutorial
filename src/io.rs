use tokio::{
    fs::File,
    io::{self, AsyncReadExt, AsyncWriteExt},
};

pub async fn read() -> io::Result<()> {
    let mut f = File::open("Cargo.toml").await?;
    let mut buf = [0; 10];

    // 读取10字节
    let n = f.read(&mut buf[..]).await?;

    println!("读取的数据：{:?}", &buf[..n]);

    Ok(())
}

pub async fn read_to_end() -> io::Result<()> {
    let mut f = File::open("Cargo.toml").await?;
    let mut buf = Vec::new();

    // 读取整个文件
    f.read_to_end(&mut buf).await?;

    Ok(())
}

pub async fn write() -> io::Result<()> {
    let mut file = File::create("/tmp/foo.txt").await?;

    let n = file.write("Hello, 世界".as_bytes()).await?;

    println!("写入 {} 字节", n);
    Ok(())
}

pub async fn write_all() -> io::Result<()> {
    let mut file = File::create("/tmp/foo.txt").await?;

    file.write_all("Hello, 世界".as_bytes()).await?;

    Ok(())
}

pub async fn copy() -> io::Result<()> {
    let mut reader: &[u8] = "Hello, 世界".as_bytes();
    let mut file = File::create("/tmp/foo.txt").await?;

    io::copy(&mut reader, &mut file).await?;

    Ok(())
}
